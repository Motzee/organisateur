document.addEventListener('DOMContentLoaded', () => {

    let theMap = document.getElementById("map") ;
    if(!!theMap) {
        var lat = 45.9590;
        var lon = 5.3406;
        var zoom = 14 ;

        /* Création de la carte via leaflet + mapbox */
        var map = L.map('map').setView([lat, lon], zoom);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 20,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoibW90emVlIiwiYSI6ImNreDlmc2kxaTB5dGoybm1uMDBmZTc5d2QifQ.2VUEb6Um0_Dkxq94fSziHA'
        }).addTo(map);

        const url = "/annuaireIRL/search" ;
        const data = {} ;
        const params = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: data,
            method: "POST"
        }

        fetch(url, params).then(data=>{return data.json()})
        .then(data=>{
            if(data.success) {
                let adresses = data.data ;
                for (const anAdress of adresses) {
                    console.log(anAdress) ;
                    //add marker and its popup
                    let marker = L.marker([anAdress.latitude, anAdress.longitude]).addTo(map);
                    marker.bindPopup("<h1>" + anAdress.title +"</h1><p>" + anAdress.address +", " + anAdress.zip + " " + anAdress.city + "</p><p>" + anAdress.phone + "</p><p>" + anAdress.hours + "</p>");
                }
            } else {
                console.error("the AJAX POST request comes back with a fail status :-/") ;
            }
            
        }).catch(error=>console.log(error)) ;

        function onMapClick(e) {
            alert("You clicked the map at " + e.latlng);
        }

        map.on('click', onMapClick);
    }
}) ;