document.addEventListener('DOMContentLoaded', () => {
    var calendarEl = document.getElementById('calendar-holder');

    if(!!calendarEl) {
        var calendar = new FullCalendar.Calendar(calendarEl, {
            defaultView: 'dayGridMonth',
            editable: true,

            /* frenchisation */
            locale: 'fr',
            firstDay: 1,
            buttonText: {
                today:    'voir Aujourd\'hui',
                month:    'mois',
                week:     'semaine',
                day:      'jour',
                list:     'liste'
            },
            allDayText: 'journée',
            /* END frenchisation */

            /* get events */
            eventSources: [
                {
                    url: "/fc-load-events",
                    method: "POST",
                    extraParams: {
                        filters: JSON.stringify({})
                    },
                    failure: () => {
                        alert("Woops, les événements à afficher sur le calendrier n'ont pas pu être récupérés… Peut-être qu'il n'y en a pas pour la période donnée ?");
                    },
                },
            ],
            /* END get events */

            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay',
            },
            plugins: [ 'interaction', 'dayGrid', 'timeGrid' ], // https://fullcalendar.io/docs/plugin-index
            timeZone: 'UTC',
        });

        calendar.render();
    }
});