document.addEventListener('DOMContentLoaded', () => {
    // media controllers
    const playPause = document.querySelector("#play-stop");

    if(!!playPause) {
        const backward = document.querySelector("#backward");
        const forward = document.querySelector("#forward");

        // record player animation
        const circleBig = document.querySelector("#circle-bg");
        const circleSm = document.querySelector("#circle-sm");

        // playing song
        const songArtist = document.querySelector("#song-artist");
        const songTitle = document.querySelector("#song-title");
        const songAlbum = document.querySelector("#song-album");
        const audio = document.querySelector("#audio");
        const coverArt = document.querySelector("#cover");
        const musicbox = document.querySelector("#musicbox");
        const lyricsLink = document.querySelector("#lyricsLink");
        const youtubeLink = document.querySelector("#youtubeLink");

        // control button images
        let playImg = "/img/icons/play.png";
        let pauseImg = "/img/icons/pause.png";

        // default controls
        playPause.src = playImg;
        let isPlaying = true;

        const songList = [
            {
                artist: "Produce 101 (Girls)",
                title: "Pick me",
                album: "101 Trainer Girls",
                year: 2015,
                bpm: null,
                lyrics: "Les paroles iront ici",
                source: "/msk/Produce 101 Girls - Pick me.mp3",
                cover: "/img/cover/produce101-pick_me.jpg"
            },
            {
                artist: "A-Ha",
                title: "Under The Makeup",
                album: "Cast in steel",
                year: 2015,
                bpm: null,
                lyrics: "Les paroles iront ici",
                source: "/msk/A-Ha - Under The Makeup.mp3",
                cover: "/img/cover/A-ha_Cast_in_Steel.jpg"
            },
            {
                artist: "Oldelaf et Monsieur D",
                title: "Le gros ours",
                album: "Chansons cons",
                year: 2005,
                bpm: null,
                lyrics: "Les paroles iront ici",
                source: "/msk/Oldelaf et Monsieur D - Le gros ours.mp3",
                cover: "/img/cover/Chansons-cons.jpg"
            },
        ];
        // helper function
        function createEle(ele) {
            return document.createElement(ele);
        }
        function append(parent, child) {
            return parent.append(child);
        }
        // creating track list
        const ul = createEle('ul')
        function createPlayList() {
            songList.forEach((song) => {
                let li = createEle('li');

                li.classList.add("track-item");
                li.innerText = song.artist + " - " + song.title;
                append(ul,li)
            })
            append(musicbox, ul);
        }

        let songIndex = 0;
        // preloaded song
        loadMusic(songList[songIndex]);

        function loadMusic() {
            coverArt.src = songList[songIndex].cover;
            songArtist.innerText = songList[songIndex].artist;
            songTitle.innerText = songList[songIndex].title;
            songAlbum.innerText = songList[songIndex].album;
            audio.src = songList[songIndex].source;
            lyricsLink.href = "https://www.google.com/search?q=lyrics+" + songList[songIndex].artist + "+" + songList[songIndex].title ;
            youtubeLink.href = "https://www.youtube.com/results?search_query=" + songList[songIndex].artist + "+" + songList[songIndex].title ;
        }

        function playSong() {
            playPause.src = pauseImg;
            circleBig.classList.add("animate");
            circleSm.classList.add("animate");

            audio.play();
        }

        function pauseSong() {
            playPause.src = playImg;
            circleBig.classList.remove("animate");
            circleSm.classList.remove("animate");

            audio.pause();
        }

        function nextPlay() {
            songIndex++;
            if(songIndex > songList.length - 1) {
                songIndex = 0;
            }
            loadMusic(songList[songIndex]);
            playSong()
        }

        function backPlay() {
            songIndex--;
            if(songIndex < 0) {
                songIndex = songList.length - 1;
            }
            loadMusic(songList[songIndex]);
            playSong()
        }
        function playHandler() {
            isPlaying = !isPlaying;
            //console.log("Change: ",isPlaying)
            isPlaying ? pauseSong() : playSong();
        }


        // player event 
        playPause.addEventListener("click", playHandler);
        backward.addEventListener("click", backPlay);
        forward.addEventListener("click", nextPlay);

        createPlayList();
    }
});