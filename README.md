# organisateur

# commandes pratiques
## contacts de test
INSERT INTO contact (slug, lastname, firstname, birthday) VALUES 
('austen_jane', 'Austen', 'Jane', '1775-12-16'),
('pratchett_terry', 'Pratchett', 'Terry', '1948-04-28')
;


# Déploiement en prod
créer un sous-domaine qui pointe vers le dossier public
git clone
composer install
création du .env (attention si prod : c'est préfixé)
bin/console doctrine:migrations:migrate
composer require symfony/apache-pack
Cette dernière commande crée un fichier .htaccess dans le dossier public


# Citations légales
icônes issus de User Interface pack de freepik.com (merci !)