<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112181329 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, id_artist INT DEFAULT NULL, is_serie TINYINT(1) DEFAULT \'0\' NOT NULL, is_serie_nb_tomes INT DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, is_available TINYINT(1) DEFAULT \'1\' NOT NULL, format VARCHAR(255) NOT NULL, genre VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tome (id INT AUTO_INCREMENT NOT NULL, id_book_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL, cover VARCHAR(255) DEFAULT NULL, is_available TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_6B19E4F7C83F1AF1 (id_book_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tome ADD CONSTRAINT FK_6B19E4F7C83F1AF1 FOREIGN KEY (id_book_id) REFERENCES book (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tome DROP FOREIGN KEY FK_6B19E4F7C83F1AF1');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE tome');
    }
}
