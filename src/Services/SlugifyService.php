<?php

namespace App\Services;
use Symfony\Component\String\Slugger\AsciiSlugger;

class SlugifyService {

    public function slugify(String $string) {

        $slugger = new AsciiSlugger();
        return $slugger->slug(strtolower(trim($string)), '_'); ;
        
    }
}