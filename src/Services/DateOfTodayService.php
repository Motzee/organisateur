<?php

namespace App\Services;

class DateOfTodayService {

    public $datetime ;
    public $datetimestr_sql ;
    public $datestr_fr ;


    public function __construct() {
        $this->datetime = new \DateTime('now') ;
        $this->datestr_sql = date_format($this->datetime, 'Y-m-d H:i:s') ;
        $this->datestr_fr = date_format($this->datetime, 'd/m/Y') ;
    }
}