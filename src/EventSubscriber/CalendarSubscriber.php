<?php

namespace App\EventSubscriber;

use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use App\Repository\EventCalendarRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CalendarSubscriber implements EventSubscriberInterface {

    private $eventCalendarRepository;
    private $router;


    public function __construct(
        EventCalendarRepository $eventCalendarRepository,
        UrlGeneratorInterface $router
    ) {
        $this->eventCalendarRepository = $eventCalendarRepository;
        $this->router = $router;
    }


    public static function getSubscribedEvents() {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar) {
        $start = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"),   date("d"),   date("Y") - 1));
        $end = date("Y-m-d H:i:s", mktime(0, 0, 0, date("m"),   date("d"),   date("Y") + 1));

        // Modify the query to fit to your entity and needs
        // Change booking.beginAt by your start date property
        $events = $this->eventCalendarRepository
            ->createQueryBuilder('event_calendar')
            ->where('event_calendar.begin_at BETWEEN :start and :end OR event_calendar.end_at BETWEEN :start and :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult()
        ;

        foreach ($events as $anEvent) {
            // this create the events with your data (here booking data) to fill calendar
            $newEvent = new Event(
                $anEvent->getTitle(),
                $anEvent->getBeginAt(),
                $anEvent->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $newEvent->setOptions([
                'backgroundColor' => 'red',
                'borderColor' => 'red',
            ]);
            $newEvent->addOption(
                'url',
                $this->router->generate('eventcalendar_show', [
                    'id' => $anEvent->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($newEvent);
        }
    }
}