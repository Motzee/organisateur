<?php

namespace App\Entity;

use App\Repository\BookRepository;
use App\Repository\TomeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    const FORMAT_MANGA = 'Manga' ;
    const FORMAT_NOVEL = 'Roman' ;
    const FORMAT_BOOK = 'Ouvrage' ;

    const STATUS_PUBLIHING_ANNOUNCED = 'Annoncé' ;
    const STATUS_PUBLIHING_INPROGRESS = 'En cours' ;
    const STATUS_PUBLIHING_ABANDONED = 'Abandonné' ;
    const STATUS_PUBLIHING_FINISHED = 'Terminé' ;

    const STATUS_COLLECTION_GAIN = 'À acquérir' ;
    const STATUS_COLLECTION_INPROGRESS = 'En cours' ;
    const STATUS_COLLECTION_ABANDONED = 'Abandonné' ;
    const STATUS_COLLECTION_FULL = 'Complet' ;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_artist;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publishing_house;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $is_serie;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $is_serie_nb_tomes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status_publication;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status_collection;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private $is_available;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $format;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $genre;

    /**
     * @ORM\OneToMany(targetEntity=Tome::class, mappedBy="id_book")
     */
    private $tomes;

    public function __construct()
    {
        $this->tomes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIdArtist(): ?int
    {
        return $this->id_artist;
    }

    public function setIdArtist(?int $id_artist): self
    {
        $this->id_artist = $id_artist;

        return $this;
    }

    public function getPublishingHouse(): ?string
    {
        return $this->publishing_house;
    }

    public function setPublishingHouse(?string $publishing_house): self
    {
        $this->publishing_house = $publishing_house;

        return $this;
    }


    public function getIsSerie(): ?bool
    {
        return $this->is_serie;
    }

    public function setIsSerie(bool $is_serie): self
    {
        $this->is_serie = $is_serie;

        return $this;
    }

    public function getIsSerieNbTomes(): ?int
    {
        return $this->is_serie_nb_tomes;
    }

    public function setIsSerieNbTomes(?int $is_serie_nb_tomes): self
    {
        $this->is_serie_nb_tomes = $is_serie_nb_tomes;

        return $this;
    }

    public function getStatusPublication(): ?string
    {
        return $this->status_publication;
    }

    public function setStatusPublication(?string $status_publication): self
    {
        $this->status_publication = $status_publication;

        return $this;
    }

    public function getStatusCollection(): ?string
    {
        return $this->status_collection;
    }

    public function setStatusCollection(?string $status_collection): self
    {
        $this->status_collection = $status_collection;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->is_available;
    }

    public function setIsAvailable(bool $is_available): self
    {
        $this->is_available = $is_available;

        return $this;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(?string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection|Tome[]
     */
    public function getTomes(): Collection
    {
        return $this->tomes;
    }

    public function addTome(Tome $tome): self
    {
        if (!$this->tomes->contains($tome)) {
            $this->tomes[] = $tome;
            $tome->setIdBook($this);
        }

        return $this;
    }

    public function removeTome(Tome $tome): self
    {
        if ($this->tomes->removeElement($tome)) {
            // set the owning side to null (unless already changed)
            if ($tome->getIdBook() === $this) {
                $tome->setIdBook(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
        return $this->id;
    }
}
