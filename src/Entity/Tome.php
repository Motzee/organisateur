<?php

namespace App\Entity;
use App\Repository\BookRepository;
use App\Repository\TomeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TomeRepository::class)
 */
class Tome
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="tomes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_book;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $chronologic_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private $is_available;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdBook(): ?Book
    {
        return $this->id_book;
    }

    public function setIdBook(?Book $id_book): self
    {
        $this->id_book = $id_book;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getChronologicNumber(): ?int
    {
        return $this->chronologic_number;
    }

    public function setChronologicNumber(?int $chronologic_number): self
    {
        $this->chronologic_number = $chronologic_number;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->is_available;
    }

    public function setIsAvailable(bool $is_available): self
    {
        $this->is_available = $is_available;

        return $this;
    }

    public function displayedTitle() {
        $displayed_title = '' ;
        if(!empty($this->title)) {
            $displayed_title = $this->title ;
        } elseif (!empty($this->chronologic_number)) {
            $displayed_title = 'Tome '.$this->chronologic_number ;
        } else {
            $displayed_title = 'Titre inconnu' ;
        }
        return $displayed_title ;
    }

    public function book(BookRepository $bookRepository) {
        return $bookRepository->findOneById($this->getIdBook) ;
    }
}
