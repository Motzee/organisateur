<?php

namespace App\Entity;

use App\Repository\SongRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SongRepository::class)
 */
class Song
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_vo;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $bpm;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $explanations;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $is_veto;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitleVo(): ?string
    {
        return $this->title_vo;
    }

    public function setTitleVo(?string $title_vo): self
    {
        $this->title_vo = $title_vo;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getBpm(): ?int
    {
        return $this->bpm;
    }

    public function setBpm(?int $bpm): self
    {
        $this->bpm = $bpm;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getExplanations(): ?string
    {
        return $this->explanations;
    }

    public function setExplanations(?string $explanations): self
    {
        $this->explanations = $explanations;

        return $this;
    }

    public function getIsVeto(): ?bool
    {
        return $this->is_veto;
    }

    public function setIsVeto(bool $is_veto): self
    {
        $this->is_veto = $is_veto;

        return $this;
    }
}
