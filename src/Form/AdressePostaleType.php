<?php

namespace App\Form;

use App\Entity\AdressePostale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdressePostaleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('slug')
            ->add('title')
            ->add('latitude')
            ->add('longitude')
            ->add('address')
            ->add('zip')
            ->add('city')
            ->add('grouping')
            ->add('hours')
            ->add('phone')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AdressePostale::class,
        ]);
    }
}
