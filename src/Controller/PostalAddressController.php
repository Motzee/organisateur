<?php

namespace App\Controller;

use App\Entity\AdressePostale;
use App\Form\AdressePostaleType;
use App\Repository\PostalAddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/postaladdress")
 */
class PostalAddressController extends AbstractController
{
    /**
     * @Route("/", name="postaladdress_index", methods={"GET"})
     */
    public function index(PostalAddressRepository $adressePostaleRepository): Response
    {
        return $this->render('postaladdress/index.html.twig', [
            'postaladdresses' => $adressePostaleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="postaladdress_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $adressePostale = new AdressePostale();
        $form = $this->createForm(AdressePostaleType::class, $adressePostale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($adressePostale);
            $entityManager->flush();

            return $this->redirectToRoute('postaladdress_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('postaladdress/new.html.twig', [
            'postaladdress' => $adressePostale,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="postaladdress_show", methods={"GET"})
     */
    public function show(AdressePostale $adressePostale): Response
    {
        return $this->render('postaladdress/show.html.twig', [
            'postaladdress' => $adressePostale,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="postaladdress_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AdressePostale $adressePostale): Response
    {
        $form = $this->createForm(AdressePostaleType::class, $adressePostale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('postaladdress_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('postaladdress/edit.html.twig', [
            'postaladdress' => $adressePostale,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="postaladdress_delete", methods={"POST"})
     */
    public function delete(Request $request, AdressePostale $adressePostale): Response
    {
        if ($this->isCsrfTokenValid('delete'.$adressePostale->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($adressePostale);
            $entityManager->flush();
        }

        return $this->redirectToRoute('postaladdress_index', [], Response::HTTP_SEE_OTHER);
    }
}
