<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Repository\BookRepository;

class MediathequeController extends AbstractController {

    /**
     * @Route("/music", name="music_player")
     */
    public function music_player(): Response {
        return $this->render('mediatheque/music_player.twig', [

        ]);
    }

    /**
     * @Route("/bibliotheque", name="bibliotheque")
     */
    public function bibliotheque(BookRepository $bookRepository): Response {
        return $this->render('mediatheque/bibliotheque.twig', [
            "mangas"        => $bookRepository->findAvailableMangas(),
            "novels"        => $bookRepository->findAvailableNovels(),
            "explainBooks"  => $bookRepository->findAvailableExplainBooks(),
        ]);
    }
}
