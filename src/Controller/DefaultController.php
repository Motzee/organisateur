<?php

namespace App\Controller;

use App\Entity\Contact ;
use App\Services\DateOfTodayService;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController {


    /**
     * @Route("/", name="home")
     */
    public function index(DateOfTodayService $today): Response {
        return $this->render('home.twig', [
            'today' => $today->datestr_fr
        ]);
    }


}
