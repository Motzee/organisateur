<?php

namespace App\Controller;

use App\Entity\Task;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TodoListController extends AbstractController
{
    /**
     * @Route("/todolist", name="todolist")
     */
    public function index(): Response {

        //$this->denyAccessUnlessGranted('ROLE_USER');

        $user = $this->getUser();
        if($user) {
            $tasks = $this->getDoctrine()->getRepository(Task::class)->findBy(['user'=> $user->getId()], ['id' => 'DESC']) ;
        } else {
            $tasks = $this->getDoctrine()->getRepository(Task::class)->findBy(['user'=> null], ['id' => 'DESC']) ;
        }   

        $tasks_list = [] ;

        foreach($tasks as $aTask) {
            if(!isset($tasks_list[$aTask->getCategory()])) {
                $tasks_list[$aTask->getCategory()] = [$aTask] ;
            } else {
                $tasks_list[$aTask->getCategory()][] = $aTask ;
            }
        }

        return $this->render('todolist/index.html.twig', [
            'todo_list' => $tasks_list,
        ]);
    }

    /**
     * @Route("/todolist/create", name="todolist_create", methods={"POST"})
     */
    public function create(Request $request): Response {

        $task_label = trim($request->request->get('label')) ;
        if(!empty($task_label)) {
            $entityManager = $this->getDoctrine()->getManager() ;
            $newTask = new Task ;
            $newTask->setLabel($task_label) ;
            $newTask->setStatus('todo') ;
            $category = trim($request->request->get('category')) ;
            $newTask->setCategory(!empty($category) ? $category : 'homing') ;
            $user = $this->getUser();
            if($user) {
                $newTask->setUser($user);
            }
            $entityManager->persist($newTask) ;
            $entityManager->flush();
        }

        return $this->redirectToRoute('todolist') ;
    }

    /**
     * @Route("/todolist/update/{id}", name="todolist_update", methods={"PUT"})
     */
    public function update(int $id): Response {
        dd("update");
    }


    /**
     * @Route("/todolist/switchdonestatus/{id}", name="todolist_switchdonestatus")
     */
    public function switchdonestatus(int $id): Response {
        $entityManager = $this->getDoctrine()->getManager() ;
        $theTask = $entityManager->getRepository(Task::class)->find($id);
        $status = $theTask->getStatus() === 'todo' ? 'done' : 'todo' ;
        $theTask->setStatus($status) ;
        $entityManager->persist($theTask) ;
        $entityManager->flush();

        return $this->redirectToRoute('todolist') ;
    }

    /**
     * @Route("/todolist/switchbacklogstatus/{id}", name="todolist_switchbacklogstatus", methods={"PUT"})
     */
    public function switchbacklogstatus(int $id): Response {
        dd("passage ou retrait du backlog");
    }

    /**
     * @Route("/todolist/delete/{id}", name="todolist_delete")
     */
    public function delete(int $id): Response {
        $entityManager = $this->getDoctrine()->getManager() ;
        $task = $entityManager->getRepository(Task::class)->find($id);
        $entityManager->remove($task) ;
        $entityManager->flush();
        return $this->redirectToRoute('todolist') ;
    }
}
