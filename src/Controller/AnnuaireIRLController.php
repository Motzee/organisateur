<?php

namespace App\Controller;

use App\Entity\AdressePostale;
use App\Services\SlugifyService ;
use App\Repository\PostalAddressRepository;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnnuaireIRLController extends AbstractController {

    /**
     * @Route("/annuaireIRL", name="annuaireIRL")
     */
    public function index(): Response
    {
        return $this->render('annuaire_irl/index.html.twig', []);
    }


    /**
     * @Route("/annuaireIRL/create", name="adresseIRL_createForm", methods={"GET"})
     */
    public function adresseCreate(): Response {
        return $this->render('annuaire_irl/adresse_create.twig', []);
    }
    /**
     * @Route("/annuaireIRL/create", name="adresseIRL_create", methods={"POST"})
     */
    public function create(Request $request, SlugifyService $slugger): Response {

        $entityManager = $this->getDoctrine()->getManager() ;

        $req = $request->request ;

        $newAdresse = new AdressePostale ;
        $title = trim($req->get('title')) ;
        $newAdresse->setSlug($slugger->slugify($title)) ;
        $newAdresse->setTitle($title);
        $newAdresse->setLatitude(trim($req->get('latitude'))) ;
        $newAdresse->setLongitude(trim($req->get('longitude'))) ;
        $newAdresse->setAddress(trim($req->get('address'))) ;
        $newAdresse->setZip(trim($req->get('zip'))) ;
        $newAdresse->setCity(trim($req->get('city'))) ;
        $newAdresse->setGrouping(!empty($req->get('grouping')) ? trim($req->get('grouping')) : null ) ;
        $newAdresse->setHours(!empty($req->get('hours')) ? trim($req->get('hours')) : null ) ;
        $newAdresse->setPhone(!empty($req->get('phone')) ? trim($req->get('phone')) : null ) ;

        $entityManager->persist($newAdresse) ;
        $entityManager->flush();
 
        return $this->redirectToRoute('annuaireIRL') ;
    }


    /**
     * @Route("/annuaireIRL/search", name="adresseIRL_search", methods={"POST"})
     */
    public function search(Request $request, PostalAddressRepository $postalAddressRepository): JsonResponse {
        $adresses_list = $postalAddressRepository->findAll() ;
//dd($adresses_list);
        return $this->json([
            'success' => true,
            'data' => $adresses_list
       ]);
    }
}
