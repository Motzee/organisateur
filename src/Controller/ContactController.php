<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Contact ;
use App\Services\SlugifyService ;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact_index")
     */
    public function index(): Response {
        $contact_list = $this->getDoctrine()->getRepository(Contact::class)->findAll() ;
        return $this->render('contact/contact_list.twig', [
            'contact_list' => $contact_list
        ]);
    }

    /**
     * @Route("/contact/create", name="contact_create", methods={"GET", "POST"})
     */
    public function contactCreate(Request $request, SlugifyService $slugger): Response {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setSlug($slugger->slugify(trim($contact->getLastname()) . '_' . trim($contact->getFirstname()))) ;
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contact/contact_create.twig', [
            'contact' => $contact,
            'form' => $form,
        ]);
    }
    /*
     * @Route("/contact/create", name="contact_create", methods={"POST"})
     
    public function create(Request $request, SlugifyService $slugger): Response {

        $entityManager = $this->getDoctrine()->getManager() ;

        $req = $request->request ;
        dd($req);
        $newContact = new Contact ;
        $firstname = trim($req->get('firstname')) ;
        $lastname = trim($req->get('lastname')) ;
dd($firstname);

        $newContact->setSlug($slugger->slugify($lastname . '_' . $firstname)) ;
        $newContact->setLastname($lastname) ;
        $newContact->setFirstname($firstname) ;
        $newContact->setBirthday(!empty($req->get('birthday')) ? $req->get('birthday') : null ) ;
dd($newContact);
        $entityManager->persist($newContact) ;
        //on doit persister tous les objets à sauver, et on ne flushera qu'une fois
        $entityManager->flush();
 
        return $this->redirectToRoute('contact') ;
    }*/

    /**
     * @Route("/contact/{slug}", name="contact_details")
     */
    public function contactDetails(string $slug): Response {
        $contact = $this->getDoctrine()->getRepository(Contact::class)->findOneBy(['slug' => $slug]) ;

        return $this->render('contact/contact_details.twig', [
            'contact'    => $contact
        ]);
    }

    /**
     * @Route("/contact/{slug}/edit", name="contact_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contact $contact): Response
    {
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contact/contact_edit.html.twig', [
            'contact' => $contact,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{slug}", name="contact_delete", methods={"POST"})
     */
    public function delete(Request $request, Contact $contact): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contact->getSlug(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contact);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contact_index', [], Response::HTTP_SEE_OTHER);
    }
}
