<?php

namespace App\Controller;

use App\Entity\EventCalendar;
use App\Form\EventCalendarType;
use App\Repository\EventCalendarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calendar")
 */
class EventCalendarController extends AbstractController
{
    /**
     * @Route("/", name="calendar_vue", methods={"GET"})
     */
    public function vue(EventCalendarRepository $eventCalendarRepository): Response
    {
        return $this->render('event_calendar/vue.html.twig', [
            //'event_calendars' => $eventCalendarRepository->findAll(),
        ]);
    }

    /**
     * @Route("/index", name="eventcalendar_index", methods={"GET"})
     */
    public function index(EventCalendarRepository $eventCalendarRepository): Response
    {
        return $this->render('event_calendar/index.html.twig', [
            'event_calendars' => $eventCalendarRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="eventcalendar_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $eventCalendar = new EventCalendar();
        $form = $this->createForm(EventCalendarType::class, $eventCalendar, [
            //'action' => $this->generateUrl('eventcalendar_new')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eventCalendar);
            $entityManager->flush();

            return $this->redirectToRoute('eventcalendar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('event_calendar/new.html.twig', [
            'event_calendar' => $eventCalendar,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/get_availables", name="eventcalendar_getavailables", methods={"POST"})
     */
    public function getAvailables(Request $request): Response
    {
        $eventCalendar = new EventCalendar();
        $form = $this->createForm(EventCalendarType::class, $eventCalendar, [
            //'action' => $this->generateUrl('eventcalendar_new')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eventCalendar);
            $entityManager->flush();

            return $this->redirectToRoute('eventcalendar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('event_calendar/new.html.twig', [
            'event_calendar' => $eventCalendar,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="eventcalendar_show", methods={"GET"})
     */
    public function show(EventCalendar $eventCalendar): Response
    {
        return $this->render('event_calendar/show.html.twig', [
            'event_calendar' => $eventCalendar,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="eventcalendar_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EventCalendar $eventCalendar): Response
    {
        $form = $this->createForm(EventCalendarType::class, $eventCalendar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('eventcalendar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('event_calendar/edit.html.twig', [
            'event_calendar' => $eventCalendar,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="eventcalendar_delete", methods={"POST"})
     */
    public function delete(Request $request, EventCalendar $eventCalendar): Response
    {
        if ($this->isCsrfTokenValid('delete'.$eventCalendar->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($eventCalendar);
            $entityManager->flush();
        }

        return $this->redirectToRoute('eventcalendar_index', [], Response::HTTP_SEE_OTHER);
    }
}
