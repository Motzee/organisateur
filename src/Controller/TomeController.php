<?php

namespace App\Controller;

use App\Entity\Tome;
use App\Entity\Book;
use App\Form\TomeType;
use App\Repository\TomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tome")
 */
class TomeController extends AbstractController
{
    /**
     * @Route("/", name="tome_index", methods={"GET"})
     */
    public function index(TomeRepository $tomeRepository): Response
    {
        return $this->render('tome/index.html.twig', [
            'tomes' => $tomeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tome_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $book = $this->getDoctrine()->getRepository(Book::class)->findOneById(1) ;

        $tome = new Tome();
        $tome->setIdBook($book);
        $form = $this->createForm(TomeType::class, $tome);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tome);
            $entityManager->flush();

            return $this->redirectToRoute('tome_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tome/new.html.twig', [
            'tome' => $tome,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="tome_show", methods={"GET"})
     */
    public function show(Tome $tome): Response
    {
        return $this->render('tome/show.html.twig', [
            'tome' => $tome,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tome_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Tome $tome): Response
    {
        $form = $this->createForm(TomeType::class, $tome);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tome_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('tome/edit.html.twig', [
            'tome' => $tome,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="tome_delete", methods={"POST"})
     */
    public function delete(Request $request, Tome $tome): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tome->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tome);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tome_index', [], Response::HTTP_SEE_OTHER);
    }
}
